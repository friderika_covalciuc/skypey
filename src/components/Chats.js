import React, { Component } from 'react';
import './chats.scss';

const Chat = ({ message }) => {
    const { text, is_user_msg } = message;
    return (
        <span className={`Chat ${is_user_msg ? "is-user-msg" : ""} `}>
            {text}
        </span>
    );
};

class Chats extends Component {

    constructor(props) {
        super(props);
        this.chatsRef = React.createRef();
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom = () => {
        // update the scroppTop property to match the scrollHeight
        // this.chatsRef.current.scrollTop = this.chatsRef.current.scrollHeight;
        this.chatsRef.scrollIntoView({ behavior: 'smooth' });
    }

    render() {
        return (
            <div className="Chats"   >
                {this.props.messages.map(message => (
                    <Chat message={message} key={message.number} />
                ))}

                <div ref={el => { this.chatsRef = el }}>
                </div>
            </div>
        );
    }
}
// TODO: scroll to bottom only when sending message

export default Chats;