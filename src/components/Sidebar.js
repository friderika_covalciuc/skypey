import React from 'react';
import User from '../containers/User';
import './sidebar.scss';

const Sidebar = ({ contacts }) => {
    // Map over the contacts prop and render a user component for each contact
    return (
        <aside className="aside Sidebar">
            {contacts.map(contact => <User user={contact} key={contact.user_id} />)}
        </aside>
    );
};

export default Sidebar;