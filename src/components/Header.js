import React from 'react';
import './header.css';

const Header = ({ user }) => {
    const { name, status } = user;

    return ( 
        <header className="Header">
            <h2 className="Header__name">{name}</h2>
            <p className="Header__status">{status}</p>
        </header>
    );
};

export default Header;