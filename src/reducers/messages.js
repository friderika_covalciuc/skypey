import { getMessages } from '../static-data';
import { SEND_MESSAGE } from '../constants/action-types';
import _ from 'lodash';

export default function messages(state = getMessages(10), action) {

    switch (action.type) {
        case SEND_MESSAGE:
            const { message, userId } = action.payload;
            // allUserMsgs = obj that contains all the user's messages
            const allUserMsgs = state[userId];
            // Pop to extract the max index of the messages
            const number = +_.keys(allUserMsgs).pop() + 1;

            return {
                ...state,
                [userId]: {
                    ...allUserMsgs,
                    [number]: {
                        number,
                        text: message,
                        is_user_msg: true
                    }
                }
            };

        default:
            return state;

    }
}