import { SET_ACTIVE_USER_ID } from '../constants/action-types'

export default function activeUserId(state = null, action) {
    switch (action.type) {
        case SET_ACTIVE_USER_ID:
            // return the ID of the user clicked
            return action.payload;
        default:
            return state;
    }
};

