import React from 'react';
import './messageInput.css';
import store from '../store';
import { setTypingValue, sendMessage } from '../actions';

const MessageInput = ({ value }) => {

    // First retrieve the current state object
    const state = store.getState();
    const handleChange = e => {
        store.dispatch(setTypingValue(e.target.value));
    };

    const handleSubmit = e => {
        e.preventDefault();
        const { typing, activeUserId } = state;
        store.dispatch(sendMessage(typing, activeUserId));
    }

    return (
        <form className="Message" onSubmit={handleSubmit} >
            <input
                className="Message__input"
                onChange={handleChange}
                value={value}
                placeholder="Type a message here"
            />
        </form>
    );
};

export default MessageInput;