import _ from 'lodash';
import React from 'react';
import store from '../store';
import Header from '../components/Header';
import Chats from '../components/Chats';
import MessageInput from '../containers/MessageInput';
import './chatwindow.css';

const ChatWindow = ({ activeUserId }) => {

    const state = store.getState();
    const activeUser = state.contacts[activeUserId];
    // state.messages = all the messages for every user contact
    // state.messages[activeUserId]= fetch the messages for the clicked(active) user
    const activeMsgs = state.messages[activeUserId];
    const typing = state.typing;

    // Chats will take the list of messages from the state object, map over them and render them
    return (
        <div className="ChatWindow">
            <Header user={activeUser} />
            <Chats messages={_.values(activeMsgs)} />
            <MessageInput value={typing} />
        </div>
    );
};

export default ChatWindow;