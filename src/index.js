import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import * as serviceWorker from './serviceWorker';
import store from "./store";
// import _ from 'lodash';



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA

const render = () => {
    fancyLog();
    return ReactDOM.render(<App />, document.getElementById('root'));
}

render();
store.subscribe(render);
serviceWorker.unregister();

function fancyLog() {
    console.log("%c Rendered with 👉 👉👇", "background: purple; color: #FFF");
    console.log(store.getState());

    // let arrayOfObj = _.values(store.getState().contacts);
    // console.log(typeof arrayOfObj);
    // console.log(arrayOfObj[1].email);
}