import React from 'react';
import Header from '../../src/components/Header';
import { shallow } from 'enzyme';

describe('<Header />', function (){

    this.getComponent = () => {
        return <Header user={this.activeUser} />;
    }

    beforeEach(() => {
        this.activeUser = {
            name : 'Test Name',
            status : 'Test Status'
        };
    })

    it('Should render <Header /> username ', () => {
        const wrapper = shallow(this.getComponent());

        const h2 = wrapper.find('h2');
        expect(h2.text()).to.equal('Test Name');
    });
});